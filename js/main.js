		var clock;
		
		$(document).ready(function() {
			var clock;

			clock = $('.clock').FlipClock({
		        clockFace: 'DailyCounter',
		        autoStart: false,
		        callbacks: {
		        	stop: function() {
		        		$('.time-run').hide();
		        		$('.time-over').show();
		        	}
		        }
		    });
				    
		    clock.setTime(600);
		    clock.setCountdown(true);
		    clock.start();

            $('#open-nav').click(function(){
                $("#main-menu").css('width', '100%');
            });
            $('#close-menu').click(function(){
                 $("#main-menu").css('width', '0%');
            });

		$(".moveto").on("click", function (event) {
			//отменяем стандартную обработку нажатия по ссылке
			event.preventDefault();

			//забираем идентификатор бока с атрибута href
			var id  = $(this).attr('href'),

			//узнаем высоту от начала страницы до блока на который ссылается якорь
				top = $(id).offset().top;
			
			//анимируем переход на расстояние - top за 1500 мс
			$('body,html').animate({scrollTop: top}, 1500);
		});

		});